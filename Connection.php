<?php

class Connection{
    private $host = "localhost";
    private $user = "root";
    private $password = "";
    private $db = "projek_pdo";
    private $connect;

    public function __construct()
    {
        $connstring = "mysql:host=".$this->host.";dbname=".
        $this->db.";charset=utf8";
        try{
            $this->connect = new PDO($connstring,$this->user,
            $this->password);
            $this->connect->setAttribute(PDO::ATTR_ERRMODE,
            PDO::ERRMODE_EXCEPTION);
            //echo "Koneksi Berhasil";
        }catch(Exception $e){
            $this->connect = "Koneksi Error";
            echo "ERROR: ". $e->getMassage();
        }
    }

    public function connection()
    {
        return $this->connect;
    }
}
